import 'package:flutter/material.dart';
import 'package:inversion_cursor/inversion_cursor.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InversionCursor(
              child: Text(
                'Hello World!',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.black),
              ),
            ),
            const SizedBox(height: 80),
            InversionCursor(
              child: Text(
                'Hello World!',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
              ),
            ),
            const SizedBox(height: 80),
            InversionCursor(
              child: Text(
                'Hello World!',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.green),
              ),
            ),
            InversionCursor(
              child: Text(
                'Hello World!',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.red),
              ),
            ),
            InversionCursor(
              child: Text(
                'Hello World!',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.blue),
              ),
            ),
            InversionCursor(
              child: Text(
                'Hello World!',
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.yellow),
              ),
            ),
            const SizedBox(height: 80),
            InversionCursor(
              color: Colors.red,
              radius: 100,
              child: Image.network(
                'https://iso.500px.com/wp-content/uploads/2016/03/stock-photo-142984111.jpg',
                height: 300,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
