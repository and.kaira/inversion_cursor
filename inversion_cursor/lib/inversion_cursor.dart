library inversion_cursor;

import 'package:flutter/material.dart';

class InversionCursor extends StatefulWidget {
  final Widget child;
  final double radius;
  final Color color;

  const InversionCursor({
    Key? key,
    required this.child,
    this.radius = 40,
    this.color = Colors.white,
  }) : super(key: key);

  @override
  State<InversionCursor> createState() => _InversionCursorState();
}

class _InversionCursorState extends State<InversionCursor> with SingleTickerProviderStateMixin {
  static late AnimationController _controller;

  static OverlayEntry? _overlayEntry;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
      reverseDuration: const Duration(milliseconds: 200),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  static final ValueNotifier<double> _radiusNotifier = ValueNotifier<double>(0.0);
  static final ValueNotifier<Color> _colorNotifier = ValueNotifier<Color>(Colors.white);
  static final ValueNotifier<Offset> _offsetNotifier = ValueNotifier<Offset>(Offset.zero);

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (pointer) {
        _radiusNotifier.value = widget.radius;
        _offsetNotifier.value = pointer.position;
        _colorNotifier.value = widget.color;
        _controller.forward();

        _insertOverlay(context);
      },
      cursor: SystemMouseCursors.none,
      onHover: (pointer) => _offsetNotifier.value = pointer.position,
      onExit: (pointer) {
        _controller.reverse();
      },
      child: widget.child,
    );
  }

  void _insertOverlay(BuildContext context) {
    if (_overlayEntry == null) {
      _overlayEntry = _getOverlayEntry;
      return Overlay.of(context).insert(
        _overlayEntry!,
      );
    } else {
      _overlayEntry = _getOverlayEntry;
    }
  }

  OverlayEntry get _getOverlayEntry {
    return OverlayEntry(
      builder: (context) {
        return AnimatedBuilder(
          animation: Listenable.merge([
            _controller,
            _offsetNotifier,
            _radiusNotifier,
            _colorNotifier,
          ]),
          builder: (context, _) {
            return CustomPaint(
              foregroundPainter: _InversionCursorPaint(
                value: _controller.value,
                maxRadius: _radiusNotifier.value,
                offset: _offsetNotifier.value,
                color: _colorNotifier.value,
              ),
            );
          },
        );
      },
    );
  }
}

class _InversionCursorPaint extends CustomPainter {
  final double value;
  final double maxRadius;
  final Offset offset;
  final Color color;

  _InversionCursorPaint({
    required this.value,
    required this.maxRadius,
    required this.offset,
    required this.color,
  });

  @override
  void paint(Canvas canvas, Size size) {
    size = Size(maxRadius, maxRadius);
    Paint paint = Paint()
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..blendMode = BlendMode.difference
      ..color = color;
    canvas.drawCircle(offset, maxRadius * value, paint);
  }

  @override
  bool shouldRepaint(_InversionCursorPaint oldDelegate) {
    return oldDelegate.value != value || oldDelegate.offset != offset;
  }
}
