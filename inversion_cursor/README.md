<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

TODO: A package that helps you to easily, create and customise inversion animation for cursor.
know whether this package might be useful for them.

## Features

Adds an inversion animation on the cursor, on the selected widget.

## Usage
A package that helps you to easily, create and customise inversion animation for cursor.

```dart
Column(
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
    InversionCursor(
      child: Text(
        'Hello World!',
        style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.black),
      ),
    ),
    const SizedBox(height: 80),
    InversionCursor(
    child: Text(
        'Hello World!',
        style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
      ),
    ),
    const SizedBox(height: 80),
    InversionCursor(
      child: Text(
        'Hello World!',
        style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.green),
      ),
    ),
    InversionCursor(
      child: Text(
        'Hello World!',
        style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.red),
      ),
    ),
    InversionCursor(
      child: Text(
        'Hello World!',
        style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.blue),
      ),
    ),
    InversionCursor(
      child: Text(
        'Hello World!',
        style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.yellow),
      ),
    ),
    const SizedBox(height: 80),
    InversionCursor(
      color: Colors.red,
      radius: 100,
      child: Image.network(
      'https://iso.500px.com/wp-content/uploads/2016/03/stock-photo-142984111.jpg',
      height: 300,
      ),
    ),
  ],
),
```

```video

```

## Additional information
I plan to add more in-depth customisation in the future